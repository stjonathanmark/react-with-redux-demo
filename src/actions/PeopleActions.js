export const CREATE_PERSON = 'people:create-person';
export const UPDATE_PERSON_FORM = 'people:update-person-form';
export const UPDATE_PERSON = 'people:update-person';
export const DELETE_PERSON = 'people:delete-person';
export const CHANGE_MODE = 'people:change-mode';

export function createPerson(person) {
    return {
        type: CREATE_PERSON,
        payload: { person, mode: 'display' }
    };
}

export function updatePersonForm(person) {
    return {
        type: UPDATE_PERSON_FORM,
        payload: { person }
    };
}

export function updatePerson(id, person) {
    return {
        type: UPDATE_PERSON,
        payload: { id, person, mode: 'display' }
    }
}

export function deletePerson(id) {
    return {
        type: DELETE_PERSON,
        payload: { id }
    }
}

export function changeMode(mode, id = null) {
    return {
        type: CHANGE_MODE,
        payload: { mode, id }
    }
}