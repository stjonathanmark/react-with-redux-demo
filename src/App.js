import React, { Component } from 'react';
import People from './components/PeopleComponent';
import PersonForm from './components/PersonFormComponent';
import './App.css';

class App extends Component {
  render() {
    return (
      <div id="app">
        <PersonForm></PersonForm>
        <People></People>
      </div>
    );
  }
}

export default App;
