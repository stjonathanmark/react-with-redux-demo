import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeMode, deletePerson } from '../actions/PeopleActions';

class People extends Component {
    onChangeMode = (evt) => {
        let parent = evt.target.parentElement;

        let idx = (parent.nodeName.toLowerCase() === 'li') ? parent.getAttribute('data-index') : null;
        let mode = idx ? 'update' : 'create';

        this.props.onChangeMode(mode, idx);
    }

    onDeletePerson = (evt) => {
        let idx = evt.target.parentElement.getAttribute('data-index');
        this.props.onDeletePerson(idx);
    }
    render() {

        const createBtn = (this.props.people.mode === 'display') ? <button onClick={this.onChangeMode}>Create New Person</button> : null;
        const editBtns = (this.props.people.mode === 'display')
            ? (
                <React.Fragment>
                    <button onClick={this.onDeletePerson}>Delete</button> &nbsp;
                    <button onClick={this.onChangeMode}>Update</button> &nbsp;
                </React.Fragment>
            ) : null;
        return (
            <section id="Persons">
                <h3>Behold the People</h3>
                {createBtn}
                <ul>
                    {
                        this.props.people.list.map((person, index) => {
                            return (
                                <li key={index} data-index={index}>
                                    {editBtns}
                                    {person}
                                </li>
                            );
                        })
                    }
                </ul>
            </section>
        );
    }
}

const mapState = (state) => {
    return {
        people: state.people
    };
}

const mapActions = {
    onChangeMode: changeMode,
    onDeletePerson: deletePerson
};

export default connect(mapState, mapActions)(People);