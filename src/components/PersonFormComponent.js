import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createPerson, updatePerson, updatePersonForm, changeMode } from '../actions/PeopleActions';

class PersonForm extends Component {
    onChangeMode = () => {
        this.props.onChangeMode('display');
    }

    onUpdatePersonForm = (evt) => {
        this.props.onUpdatePersonForm(evt.target.value);
    }

    onCreatePerson = () => {
        this.props.onCreatePerson(this.props.people.person);
    }

    onUpdatePerson = () => {
        this.props.onUpdatePerson(this.props.people.id, this.props.people.person);
    }

    render() {
        const mode = this.props.people.mode;

        if (mode === 'display') {
            return null;
        }

        const modeText = mode.charAt(0).toUpperCase() + mode.slice(1);

        return (
            <section id='form'>
                <h3>{modeText} Person</h3>
                <label><b>Person:</b></label>
                <br />
                <input type="text" name="person" onChange={this.onUpdatePersonForm} value={this.props.people.person} />
                <br />
                <button onClick={mode === 'create' ? this.onCreatePerson : this.onUpdatePerson}>{modeText}</button> &nbsp;
                <button onClick={this.onChangeMode}>Cancel</button>
            </section>
        );
    }
}

const mapProps = (state) => {
    return {
        people: state.people
    };
}

const mapActions = {
    onCreatePerson: createPerson,
    onUpdatePerson: updatePerson,
    onUpdatePersonForm: updatePersonForm,
    onChangeMode: changeMode
};

export default connect(mapProps, mapActions)(PersonForm);
