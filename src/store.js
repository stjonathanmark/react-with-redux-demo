import { createStore } from 'redux';
import { allReducers } from './reducers/RootReducer';

const store = createStore(allReducers, {
    people: {
        list: ['John', 'Jane'],
        person: null,
        id: null,
        mode: 'display'
    }
}, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;