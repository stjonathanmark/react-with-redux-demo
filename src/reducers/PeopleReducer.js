import { CREATE_PERSON, UPDATE_PERSON, UPDATE_PERSON_FORM, DELETE_PERSON, CHANGE_MODE } from '../actions/PeopleActions';

export function peopleReducer(currentState = {}, action) {
    let newState = getStateCopy(currentState);

    switch (action.type) {
        case CREATE_PERSON:
            createPerson(newState, action);
            break;
        case UPDATE_PERSON_FORM:
            updatePersonForm(newState, action);
            break;
        case UPDATE_PERSON:
            updatePerson(newState, action);
            break;
        case DELETE_PERSON:
            deletePerson(newState, action);
            break;
        case CHANGE_MODE:
            changeMode(newState, action);
            break;
        default:
            break;
    }

    return newState;
}

function getStateCopy(state) {
    return JSON.parse(JSON.stringify(state));
}

function createPerson(state, action) {
    state.list.push(action.payload.person);
    changeMode(state, action);
}

function updatePersonForm(state, action) {
    state.person = action.payload.person;
}

function updatePerson(state, action) {
    state.list.splice(action.payload.id, 1, action.payload.person);
    changeMode(state, action);
}

function deletePerson(state, action) {
    state.list.splice(action.payload.id, 1);
}

function changeMode(state, action) {
    state.mode = action.payload.mode;
    if (state.mode === 'update') {
        state.id = action.payload.id;
        state.person = state.list[state.id];
    } else {
        state.id = null;
        state.person = '';
    }
}
