import { combineReducers } from 'redux';
import { peopleReducer } from './PeopleReducer';

export const allReducers = combineReducers({
    people: peopleReducer
});