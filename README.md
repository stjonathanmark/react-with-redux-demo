# Description
This is a demo app that is created with React and Redux.

# Instructions
1. To run this application you will need to download and install **NodeJs**. [Download NodeJs](https://nodejs.org)
2. Open the NodeJs command line tool and set it's path to the **'Application Root'** folder where you placed it on your computer after downloading/cloning repository
3. Type and run the command **'npm install'** in the NodeJs command line tool to install all the dependencies from the **'package.json'** file
4. Type and run the command **'npm start'** in the NodeJs command line tool to run the app and automatically open it in your default browser
5. Enjoy!!!!!!!